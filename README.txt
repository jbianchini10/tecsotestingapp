Utilic� Spring Tool Suite 4 para el desarrollo y Java 1.8
La aplicaci�n funciona sobre MySQL Server. Para poder realizar las pruebas es necesario crear una sesi�n
con las siguientes credenciales:
user:root
pass:root
port:3306

y crear una base de datos de nombre 'accounting'. Todas estas propiedades pueden cambiarse desde 
src/main/resources/application.properties

Una vez creada la base, ejecutar los test para mapear las entidades y popular la base con datos de prueba.
Tambi�n puede ejecutarse directamente la aplicaci�n como Spring Boot Application y las entidades se mapean
solas.

Utilic� Postman para probar los endpoints rest.

GET: /account (int): Consultar cuenta por numero de cuenta. 
GET: /accounts (): Consultar todas las cuentas de la base.
POST: /account (JSON): Guarda una cuenta nueva en la base de datos.
DELETE: /account (int): Borra una cuenta por su numero de cuenta.
POST: /move (JSON): Guarda un nuevo movimiento en la base.
GET: /moves (int): Consulta todos los movimientos relacionados al numero de cuenta ingresado.

Los m�todos de la capa de servicio lanzan excepciones propias para indicar errores al usuario.
El resto de las excepciones son manejadas por el ApiExceptionHandler y devuelven un formato estandar de mensaje.

Los mapeos entre DTOs de la API y entidades los hace la clase DozerCustomConverter del paquete mapper.

M�s informaci�n sobre los m�todos y sus par�metros en doc/index.html. 