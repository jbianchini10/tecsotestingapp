package com.tecso.accounting.web.api;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tecso.accounting.model.entity.Account;
import com.tecso.accounting.model.entity.Move;
import com.tecso.accounting.model.service.AccountService;
import com.tecso.accounting.model.service.MoveService;

@RestController
public class ApplicationApi {

	@Autowired
	AccountService accountService;

	@Autowired
	MoveService moveService;

	@Autowired
	Mapper mapper;
	
	/**
	 * Consulta una cuenta mediante su numero de cuenta
	 * @param number Numero de cuenta
	 * @return AccountDTO
	 * @throws Exception
	 */
	@GetMapping("/account")
	public AccountDTO getAccountByNumber(@RequestBody int number) throws Exception {
		return mapper.map(accountService.getByNumber(number), AccountDTO.class);
	}

	/**
	 * Obtiene todas las cuentas de la base de datos.
	 * @return Lista de accountDTO
	 */
	@GetMapping("/accounts")
	public List<AccountDTO> listAllAccounts() {
		List<Account> accounts = accountService.listAll();
		return this.toDTOList(accounts, AccountDTO.class);

	}

	/**
	 * Guarda una cuenta nueva en la base de datos.
	 * @param accountDto DTO de la nueva cuenta.
	 * @return DTO para devolver a capa REST.
	 * @throws Exception
	 */
	@PostMapping("/account")
	public AccountDTO saveAccount(@RequestBody @Valid AccountDTO accountDto) throws Exception {
		Account account = mapper.map(accountDto, Account.class);
		return mapper.map(accountService.save(account), AccountDTO.class);

	}

	/**
	 * Borra una cuenta de la base de datos mediante su numero de cuenta.
	 * @param accountNumber Numero de cuenta.
	 * @return Respuesta para capa REST
	 * @throws Exception
	 */
	@DeleteMapping("/account")
	public ResponseEntity<ResponseMessage> deleteAccount(@RequestBody int accountNumber) throws Exception {
		// Borrar una cuenta por su numero de cuenta.

		accountService.delete(accountNumber);

		return acceptResponse("La cuenta " + accountNumber + " ha sido eliminada.");

	}

	/**
	 * Guarda un movimiento nuevo en la base de datos.
	 * @param moveDto DTO del movimiento nuevo.
	 * @return MoveDTO
	 * @throws Exception
	 */
	@PostMapping("/move")
	public MoveDTO saveMove(@RequestBody @Valid MoveDTO moveDto) throws Exception {
		// Crear movimiento
		Move move = mapper.map(moveDto, Move.class);
		return mapper.map(moveService.save(move), MoveDTO.class);
	}

	/**
	 * Obtiene todos los movimientos asociados a una cuenta.
	 * @param accountNumber El numero de cuenta
	 * @return Lista de MoveDTO
	 * @throws Exception
	 */
	@GetMapping("/moves")
	public List<MoveDTO> getMoveByAccountNumber(@RequestBody int accountNumber) throws Exception {
		// Obtener todos los movimientos de una cuenta
		List<Move> list = moveService.listByAccountNumber(accountNumber);
		return toDTOList(list, MoveDTO.class);
	}

	/**
	 * Crea un ResponseEntity para devolver a la capa REST.
	 * @param message Mensaje para mostrar a usuario.
	 * @return ResponseEntity
	 */
	private ResponseEntity<ResponseMessage> acceptResponse(String message) {
		//Devuelve una entidad de respuesta para la capa rest
		ResponseMessage rsp = new ResponseMessage(message, null, null, null);
		return new ResponseEntity<ResponseMessage>(rsp, HttpStatus.ACCEPTED);
	}
	
	
	private <T, O> List<O> toDTOList(List<T> list, Class<O> class2) {
		/*
		 * Metodo auxiliar para convertir una lista de T en una lista de O
		 */
		ArrayList<O> retList = new ArrayList<>();
		for (T t : list) {
			retList.add(mapper.map(t, class2));
		}

		return retList;
	}

}
